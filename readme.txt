=== Spice White Label ===
Contributors:           spicethemes
Requires at least:      5.3
Requires PHP:           5.2
Tested up to:           6.7.1
Stable tag:             1.0.1
License:                GPLv2 or later
License URI:            https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

A plugin which add a new box in Theme Panel to allow you to replace the Theme name by your own branding name.

== Changelog ==

= 1.0.1 =
* Updated freemius directory and fixed warning issue.

= 1.0 =
* Updated Freemius Code.

= 0.2 =
* Updated Freemius Code.

= 0.1 =
* Initial Release.
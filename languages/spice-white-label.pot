# Copyright 2021 ...
# This file is distributed under the GNU General Public License v3 or later.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: "
"Blank WordPress Pot "
"v1.0.0\n"
"Report-Msgid-Bugs-To: "
"Translator Name "
"<translations@example."
"com>\n"
"POT-Creation-Date: "
"2025-02-05 13:52+0530\n"
"PO-Revision-Date: \n"
"Last-Translator: Your "
"Name <you@example.com>\n"
"Language-Team: Your Team "
"<translations@example."
"com>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/"
"plain; charset=UTF-8\n"
"Content-Transfer-"
"Encoding: 8bit\n"
"Plural-Forms: "
"nplurals=2; plural=n != "
"1;\n"
"X-Textdomain-Support: "
"yesX-Generator: Poedit "
"1.6.4\n"
"X-Poedit-SourceCharset: "
"UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;esc_html_e;"
"esc_html_x:1,2c;"
"esc_html__;esc_attr_e;"
"esc_attr_x:1,2c;"
"esc_attr__;_ex:1,2c;"
"_nx:4c,1,2;"
"_nx_noop:4c,1,2;_x:1,2c;"
"_n:1,2;_n_noop:1,2;"
"__ngettext:1,2;"
"__ngettext_noop:1,2;_c,"
"_nc:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 3.5\n"
"X-Poedit-"
"SearchPath-0: .\n"

#: spice-white-label-menu.php:7
#: spice-white-label.php:152
msgid "Spice White Label"
msgstr ""

#: spice-white-label.php:158
msgid "Theme Branding:"
msgstr ""

#: spice-white-label.php:160
msgid ""
"It replace Theme name in "
"the admin as metabox "
"setting"
msgstr ""

#: spice-white-label.php:164
msgid "Theme Name:"
msgstr ""

#: spice-white-label.php:166
msgid ""
"It replace the theme "
"name in Appearance > "
"Themes."
msgstr ""

#: spice-white-label.php:170
msgid "Theme Author:"
msgstr ""

#: spice-white-label.php:172
msgid ""
"It replace the theme "
"author in Appearance > "
"Themes."
msgstr ""

#: spice-white-label.php:176
msgid "Theme Author URL:"
msgstr ""

#: spice-white-label.php:178
msgid ""
"It replace the theme "
"author url in Appearance "
"> Themes."
msgstr ""

#: spice-white-label.php:182
msgid "Theme Description:"
msgstr ""

#: spice-white-label.php:184
msgid ""
"It replace the theme "
"description in "
"Appearance > Themes."
msgstr ""

#: spice-white-label.php:188
msgid ""
"Theme Screenshot URL:"
msgstr ""

#: spice-white-label.php:193
msgid "Preview Image"
msgstr ""

#: spice-white-label.php:197
msgid "Upload"
msgstr ""

#: spice-white-label.php:198
msgid "Remove Image"
msgstr ""

#: spice-white-label.php:199
msgid ""
"It replace the theme "
"screenshot in Appearance "
"> Themes. Recommended "
"size: 1200x900px"
msgstr ""

#: spice-white-label.php:205
msgid ""
"Hide The Themes Section "
"in the Customizer"
msgstr ""

#: spice-white-label.php:209
msgid "Save Changes"
msgstr ""

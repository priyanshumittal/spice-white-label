<?php
/*
* Plugin Name:          Spice White Label
* Plugin URI:           https://spicethemes.com/spice-white-label/
* Description:          A plugin which add a new box in Theme Panel to allow you to replace the Theme name by your own branding name.
* Version:              1.0.1
* Requires at least:    5.3
* Requires PHP:         5.2
* Tested up to:         6.7.1
* Author:               Spicethemes
* Author URI:           https://spicethemes.com
* License:              GPLv2 or later
* License URI:          https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain:          spice-white-label
* Domain Path:          /languages
*/

//Freemius SDK Snippet
if ( ! function_exists( 'swl_fs' ) ) {
    // Create a helper function for easy SDK access.
    function swl_fs() {
        global $swl_fs;

        if ( ! isset( $swl_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }
          
            $swl_fs = fs_dynamic_init( array(
                'id'                  => '10573',
                'slug'                => 'spice-white-label',
                'premium_slug'        => 'spice-white-label',
                'type'                => 'plugin',
                'public_key'          => 'pk_6a6aa7643a2adc4d19e6d1eb1f2a3',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'slug'           => 'spice_white_label',
                    'first-path'     => 'admin.php?page=spice_white_label',
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                ) );
        }

        return $swl_fs;
    }
  swl_fs();
}

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


final class Spice_White_Label {
    
    private static $_instance = null;

    public $theme;

    private $plugin_url;
    private $plugin_path;

    public function __construct( $widget_areas = array() ) {
        $this->plugin_url       = plugin_dir_url( __FILE__ );
        $this->plugin_path      = plugin_dir_path( __FILE__ );

        //Register admin menu
        require_once $this->plugin_path . 'spice-white-label-menu.php';

        //Load 
        add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

        $theme = wp_get_theme();
    }
    
    

    public function init() {
        add_action( 'init', array( $this, 'spice_hooks' ) );

        // Hide Themes section in the customizer as the theme name cannot be edited in it
        if ( true == get_option( 'spice_wl_hide_themes_customizer', false ) ) {
            add_action( 'customize_register', array( $this, 'remove_themes_section' ), 30 );
        }
    }

    public static function instance() {
        if ( is_null( self::$_instance ) )
            self::$_instance = new self();
        return self::$_instance;
    }

    public function spice_hooks() {
        add_action( 'spice_white_label_hook', array( $this, 'spice_white_label_box' ) );
        add_action( 'admin_init', array( $this, 'spice_white_label_register_setting' ) );
        add_filter( 'swl_theme_branding', array( $this, 'spice_white_label_get_theme_branding_settings' ) );
        add_filter( 'wp_prepare_themes_for_js', array( $this, 'spice_white_label_get_theme_branding' ) );
        add_filter( 'update_right_now_text', array( $this, 'spice_white_label_dashboard' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ) );
    }


    public function load_plugin_textdomain() {
        load_plugin_textdomain( 'spice-white-label', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }

    public static function remove_themes_section( $wp_customize ) {
        $wp_customize->remove_panel( 'themes' );
    }

    public static function get_white_label_settings() {
        $theme = wp_get_theme();
        $branding = array(
            'branding'                  => get_option( strtolower($theme->name).'_theme_branding' ),
            'name'                      => get_option( strtolower($theme->name).'_theme_name' ),
            'author'                    => get_option( strtolower($theme->name).'_theme_author' ),
            'author_url'                => get_option( strtolower($theme->name).'_theme_author_url' ),
            'description'               => get_option( strtolower($theme->name).'_theme_description' ),
            'screenshot'                => get_option( strtolower($theme->name).'_theme_screenshot' ),
            'hide_themes_customizer'    => get_option( 'spice_wl_hide_themes_customizer', false ),
        );

        return apply_filters( 'spice_white_label_settings', $branding );
    }

    public static function spice_white_label_box() {

        // Only if manage_options attr
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        // Get settings
        $settings = self::get_white_label_settings(); ?>

        <div class="divider clr"></div>

        <div class="spice-white-label-section spice-white-label-brand">
            <h3><?php esc_html_e( 'Spice White Label', 'spice-white-label' ); ?></h3>
            <div class="content-wrap">
                <form id="spice-white-label-brand-form" method="post" action="options.php">
                    <?php settings_fields( 'spice_white_label_fields' ); ?>

                    <div class="field-wrap left">
                        <label for="spice-white-label-branding"><?php esc_html_e( 'Theme Branding:', 'spice-white-label' ); ?></label>
                        <input type="text" name="spice_white_label_fields[branding]" id="spice-white-label-branding" value="<?php echo esc_attr( $settings['branding'] ); ?>">
                        <p class="desc"><?php esc_html_e( 'It replace Theme name in the admin as metabox setting', 'spice-white-label' ); ?></p>
                    </div>

                    <div class="field-wrap right">
                        <label for="spice-wl-name"><?php esc_html_e( 'Theme Name:', 'spice-white-label' ); ?></label>
                        <input type="text" name="spice_white_label_fields[name]" id="spice-wl-name" value="<?php echo esc_attr( $settings['name'] ); ?>">
                        <p class="desc"><?php esc_html_e( 'It replace the theme name in Appearance > Themes.', 'spice-white-label' ); ?></p>
                    </div>

                    <div class="field-wrap left">
                        <label for="spice-wl-author"><?php esc_html_e( 'Theme Author:', 'spice-white-label' ); ?></label>
                        <input type="text" name="spice_white_label_fields[author]" id="spice-wl-author" value="<?php echo esc_attr( $settings['author'] ); ?>">
                        <p class="desc"><?php esc_html_e( 'It replace the theme author in Appearance > Themes.', 'spice-white-label' ); ?></p>
                    </div>

                    <div class="field-wrap right">
                        <label for="spice-wl-author_url"><?php esc_html_e( 'Theme Author URL:', 'spice-white-label' ); ?></label>
                        <input type="text" name="spice_white_label_fields[author_url]" id="spice-wl-author_url" value="<?php echo esc_url( $settings['author_url'] ); ?>">
                        <p class="desc"><?php esc_html_e( 'It replace the theme author url in Appearance > Themes.', 'spice-white-label' ); ?></p>
                    </div>

                    <div class="field-wrap clear">
                        <label for="spice-wl-description"><?php esc_html_e( 'Theme Description:', 'spice-white-label' ); ?></label>
                        <textarea name="spice_white_label_fields[description]" id="spice-wl-description" rows="3"><?php echo esc_attr( $settings['description'] ); ?></textarea>
                        <p class="desc"><?php esc_html_e( 'It replace the theme description in Appearance > Themes.', 'spice-white-label' ); ?></p>
                    </div>

                    <div class="field-wrap left">
                        <label for="spice-wl-screenshot"><?php esc_html_e( 'Theme Screenshot URL:', 'spice-white-label' ); ?></label>
                        <div class="spice-wl-media-live-preview" style="display:none;">
                            <?php
                            $preview = $settings['screenshot'];
                            if ( $preview ) { ?>
                                <img src="<?php echo esc_url( $preview ); ?>" alt="<?php esc_html_e( 'Preview Image', 'spice-white-label' ); ?>" />
                            <?php } ?>
                        </div>
                        <input class="spice-wl-media-input" type="text" name="spice_white_label_fields[screenshot]" value="<?php echo esc_url( $settings['screenshot'] ); ?>">
                        <input class="spice-wl-media-upload-button button-secondary" type="button" value="<?php esc_html_e( 'Upload', 'spice-white-label' ); ?>" />
                        <a href="#" class="spice-wl-media-remove" style="display:none;"><?php esc_html_e( 'Remove Image', 'spice-white-label' ); ?></a>
                        <p class="desc"><?php esc_html_e( 'It replace the theme screenshot in Appearance > Themes. Recommended size: 1200x900px', 'spice-white-label' ); ?></p>
                    </div>

                    <div class="field-wrap clear">
                        <label for="spice-wl-hide-themes-customizer">
                            <input type="checkbox" id="spice-wl-hide-themes-customizer" name="spice_white_label_fields[hide_themes_customizer]" value="1" <?php checked( '1', $settings['hide_themes_customizer'] ); ?>>
                            <?php esc_html_e( 'Hide The Themes Section in the Customizer', 'spice-white-label' ); ?>
                        </label>
                    </div>

                    <input type="submit" name="spice_white_label_fields_save" id="submit" class="button swl-button" value="<?php esc_attr_e( 'Save Changes', 'spice-white-label' ); ?>">

                    <?php
                    // Updated notice
                    if ( isset( $_GET['settings-updated'] ) ) {
                        echo '<div class="spice-wl-settings-updated"><p>Settings updated successfully.</p></div>';
                    } ?>

                    <?php wp_nonce_field( 'spice-white-label', 'spice-white-label-nonce' ); ?>
                </form>
            </div>
        </div>

    <?php
    }

    public function spice_white_label_register_setting() {
        register_setting( 'spice_white_label_fields', 'spice_white_label_fields', array( $this, 'sanitize_white_label_settings' ) ); 
    }


    public function sanitize_white_label_settings() {
        $theme = wp_get_theme();
        if ( ! isset( $_POST['spice-white-label-nonce'] )
            && ! wp_verify_nonce( $_POST['spice-white-label-nonce'], 'spice-white-label' ) ) {
            return;
        }

        if ( ! isset( $_POST['spice_white_label_fields'] ) ) {
            return;
        }

        // Get settings
        $settings = self::get_white_label_settings();

        // Loop
        foreach( $settings as $key => $setting ) {

            if ( in_array( $key, array( 'description' ) ) ) {
                if ( isset( $_POST['spice_white_label_fields']['description'] ) ) {
                    update_option( strtolower($theme->name).'_theme_description', wp_filter_nohtml_kses( wp_unslash( $_POST['spice_white_label_fields']['description'] ) ) );
                }
            }  
            else if ( in_array( $key, array( 'hide_themes_customizer' ) ) ) {
                if ( isset( $_POST['spice_white_label_fields']['hide_themes_customizer'] ) ) {
                    update_option( 'spice_wl_hide_themes_customizer', true );
                } else {
                    update_option( 'spice_wl_hide_themes_customizer', false );
                }
            } 
            else {
                if ( isset( $_POST['spice_white_label_fields'][$key] ) ) {
                    update_option( strtolower($theme->name).'_theme_'. $key, sanitize_text_field( wp_unslash( $_POST['spice_white_label_fields'][$key] ) ) );
                }
            }
        }
 
    }


    public static function spice_white_label_get_theme_branding_settings( $return ) {
        $theme = wp_get_theme();
        if ( $setting = get_option( strtolower($theme->name).'_theme_branding' ) ) {
            $return = $setting;
        }

        return $return;
 
    }

    public static function spice_white_label_get_theme_branding( $themes ) {
        $theme = wp_get_theme();
        $key = strtolower(str_replace(' ', '-', $theme->name));
        $actual_name=$theme->name;


        if ( isset( $themes[ $key ] ) ) {

            // Get settings
            $theme_data = self::get_white_label_settings();

            // Theme naem
            if ( ! empty( $theme_data['name'] ) ) {

                $themes[ $key ]['name'] = $theme_data['name'];

                foreach ( $themes as $parent_key => $theme ) {
                    if ( isset( $theme['parent'] ) && $actual_name == $theme['parent'] ) {
                        $themes[ $parent_key ]['parent'] = $theme_data['name'];
                    }
                }
            }

            // Theme description
            if ( ! empty( $theme_data['description'] ) ) {
                $themes[ $key ]['description'] = $theme_data['description'];
            }

            // Theme author and author url
            if ( ! empty( $theme_data['author'] ) ) {
                $author_url = empty( $theme_data['author_url'] ) ? '#' : $theme_data['author_url'];
                $themes[ $key ]['author'] = $theme_data['author'];
                $themes[ $key ]['authorAndUri'] = '<a href="' . esc_url( $author_url ) . '">' . $theme_data['author'] . '</a>';
            }

            // Theme screenshot
            if ( ! empty( $theme_data['screenshot'] ) ) {
                $themes[ $key ]['screenshot'] = array( $theme_data['screenshot'] );
            }
        }

        return $themes;
 
    }

    public static function spice_white_label_dashboard( $return ) {
        $theme = wp_get_theme();
        $actual_name=$theme->name;
        // Get setting
        $theme_data = self::get_white_label_settings();

        // Add the theme name
        if ( is_admin() && $actual_name == wp_get_theme() && ! empty( $theme_data['name'] ) ) {
            return sprintf( $return, get_bloginfo( 'version', 'display' ), '<a href="themes.php">' . $theme_data['name'] . '</a>' );
        }

        // Return
        return $return;

    }

    public function load_scripts( ) {

        // CSS
        wp_enqueue_style( 'spice-white-label', plugins_url( '/assets/css/style.min.css', __FILE__ ) );

        // JS
        wp_enqueue_media();
        wp_enqueue_script( 'spice-white-label-uploader', plugins_url( '/assets/js/uploader.min.js', __FILE__ ), array( 'media-upload' ), false, true );

    }

} 
 Spice_White_Label::instance()->init();
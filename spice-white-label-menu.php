<?php
add_action('admin_menu', 'spice_white_label_menu');

function spice_white_label_menu() { 
  add_menu_page( 
      'Page Title', 
      esc_html__('Spice White Label','spice-white-label'), 
      'edit_posts', 
      'spice_white_label', 
      'spice_white_label_callback', 
      'dashicons-welcome-view-site',
      '30' 
     );
}

function spice_white_label_callback() {
  $theme = wp_get_theme();
        do_action('spice_white_label_hook');
}
?>